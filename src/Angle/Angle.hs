{-# LANGUAGE DeriveGeneric #-}

module Angle.Angle (foo) where

import qualified Data.Yaml as Y
import GHC.Generics
import Data.Aeson
import Control.Applicative
import qualified Data.ByteString.Char8 as BS

data MyPoint = MyPoint {r :: Float
                       ,f :: Float
                       ,z:: Float}
             deriving (Show, Generic)

instance FromJSON MyPoint

foo :: BS.ByteString -> Maybe MyPoint
foo ymlData = Y.decode ymlData
